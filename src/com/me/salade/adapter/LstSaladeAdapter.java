package com.me.salade.adapter;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Shader.TileMode;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.me.salade.R;
import com.me.salade.model.CommandeItems;
import com.me.salade.model.Salade;

public class LstSaladeAdapter extends ArrayAdapter<Salade> {

	private ArrayList<Salade> data;
	private LayoutInflater inflater = null;
	private Context cxt;
	private TextView txt_totalPrice;
	private TextView txt_totalCalorie;
	private ArrayList<CommandeItems> commande;

	public LstSaladeAdapter(Context cxt, int resid, ArrayList<Salade> data,
			TextView txt_totalPrice, TextView txt_totalCalorie) {
		super(cxt, resid);
		this.cxt = cxt;
		this.data = data;
		this.txt_totalPrice = txt_totalPrice;
		this.txt_totalCalorie = txt_totalCalorie;

		inflater = (LayoutInflater) cxt
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		commande = new ArrayList<CommandeItems>();
	}

	public ArrayList<CommandeItems> getCommande() {
		return commande;
	}

	public void setItems(ArrayList<Salade> data) {
		this.data = data;
		notifyDataSetChanged();
	}

	@Override
	public int getCount() {
		return data.size();
	}

	@Override
	public Salade getItem(int position) {
		return data.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(final int position, View view, ViewGroup parent) {

		Salade salade = data.get(position);

		final ViewHolder holder;

		if (view == null) {
			holder = new ViewHolder();
			view = inflater.inflate(R.layout.item_salade_adapter, null);

			holder.parentLayout = (RelativeLayout) view
					.findViewById(R.id.parentContainer);
			holder.txt_name = (TextView) view.findViewById(R.id.txt_nameItem);
			holder.txt_prix = (TextView) view.findViewById(R.id.txt_priceItem);
			holder.txt_calorie = (TextView) view
					.findViewById(R.id.txt_calorieItem);
			holder.imgSalade = (ImageView) view.findViewById(R.id.img_item);

			holder.imgQuantite = (ImageView) view
					.findViewById(R.id.img_quantite);
			holder.imgAddOrRemove = (ImageView) view
					.findViewById(R.id.img_retirerAjouter);

			view.setTag(holder);
		}

		else {
			holder = (ViewHolder) view.getTag();
		}

		holder.txt_name.setText(salade.getName());
		holder.txt_prix.setText(salade.getPrice() + " dt");
		holder.txt_calorie.setText(salade.getCalories() + " cal");

		URL url;
		try {
			url = new URL(salade.getImage());

			Bitmap bmp = BitmapFactory.decodeStream(url.openConnection()
					.getInputStream());

			Bitmap circleBitmap = Bitmap.createBitmap(bmp.getWidth(),
					bmp.getHeight(), Bitmap.Config.ARGB_8888);

			BitmapShader shader = new BitmapShader(bmp, TileMode.CLAMP,
					TileMode.CLAMP);
			Paint paint = new Paint();
			paint.setShader(shader);

			Canvas c = new Canvas(circleBitmap);
			c.drawCircle(bmp.getWidth() / 2, bmp.getHeight() / 2,
					bmp.getWidth() / 2, paint);

			holder.imgSalade.setImageBitmap(circleBitmap);
			// getRoundedCornerBitmap(bmp, 10);

		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		holder.imgQuantite.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View view) {

				System.out.println("imgQuantite is Clicked *******");

				int quantite;
				holder.imgAddOrRemove.setTag("1");
				if (holder.imgQuantite.getTag().equals("2")) {
					quantite = 2;
					holder.imgQuantite.setTag("1");
					holder.imgQuantite.setBackgroundDrawable(cxt.getResources()
							.getDrawable(R.drawable.simple));
					holder.imgAddOrRemove.setBackgroundDrawable(cxt
							.getResources().getDrawable(R.drawable.retirer));
					System.out.println("img IS double *****");
				} else {
					quantite = 1;
					holder.imgQuantite.setTag("2");
					holder.imgQuantite.setBackgroundDrawable(cxt.getResources()
							.getDrawable(R.drawable.img_double));
				}

				Salade current = getItem(position);

				CommandeItems comm = new CommandeItems(current, quantite);

				int inCommande = -1;
				for (int i = 0; i < commande.size(); i++) {

					if (commande.get(i).getSalade().getId() == comm.getSalade()
							.getId()) {
						inCommande = i;
					}
				}

				if (inCommande == -1) {
					commande.add(comm);
				} else {
					commande.get(inCommande).setQuantite(quantite);
				}

				float price = 0;
				float calories = 0;
				for (int i = 0; i < commande.size(); i++) {

					price = price
							+ (Float.parseFloat(commande.get(i).getSalade()
									.getPrice()) * commande.get(i)
									.getQuantite());
					calories = calories
							+ Integer.parseInt((commande.get(i).getSalade()
									.getCalories()))
							* commande.get(i).getQuantite();
				}

				txt_totalPrice.setText(price + " dt");
				txt_totalCalorie.setText(calories + " cal");
				if (comm.getQuantite() > 0)
					holder.parentLayout
							.setBackgroundResource(R.drawable.bande_element_2);
				else
					holder.parentLayout
							.setBackgroundResource(R.drawable.bande_element1);
			}
		});

		holder.imgAddOrRemove.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				System.out.println("imgRemove is Clicked *******");

				boolean ajouter;
				int quantite;
				if (holder.imgAddOrRemove.getTag().equals("0")) {
					System.out.println("img IS ajouter *****");
					ajouter = true;
					holder.imgAddOrRemove.setBackgroundDrawable(cxt
							.getResources().getDrawable(R.drawable.retirer));
					holder.parentLayout
							.setBackgroundResource(R.drawable.bande_element_2);
					holder.imgAddOrRemove.setTag("1");

					Salade current = getItem(position);

					int inCommande = -1;
					for (int i = 0; i < commande.size(); i++) {

						if (commande.get(i).getSalade().getId() == current
								.getId()) {
							inCommande = i;
							quantite = commande.get(i).getQuantite();
						}
					}

					if (inCommande == -1) {
						CommandeItems comm = new CommandeItems(current, 1);
						commande.add(comm);
					} else {
						System.out.println("not in the right place");
						commande.get(inCommande).setQuantite(2);
					}

				} else {
					System.out.println("img is remove");
					holder.imgAddOrRemove.setTag("0");

					ajouter = false;

					Salade current = getItem(position);
					quantite = -1;
					int inCommande = -1;
					for (int i = 0; i < commande.size(); i++) {

						if (commande.get(i).getSalade().getId() == current
								.getId()) {
							inCommande = i;
							quantite = commande.get(i).getQuantite();
						}
					}

					if (inCommande == -1) {
						CommandeItems comm = new CommandeItems(current, 1);
						commande.add(comm);
					} else {

						// System.out.println("size Commande before remove  :" +
						// commande.size());
						// commande.remove(inCommande);
						// System.out.println("size Commande after remove  :" +
						// commande.size());
						// CommandeItems comm = new CommandeItems(current, 1);
						// commande.add(comm);
						System.out.println("Quantite else  : " + quantite);
						if (quantite == 2) {
							commande.get(inCommande).setQuantite(1);
							holder.imgAddOrRemove.setTag("1");

							holder.imgAddOrRemove.setBackgroundDrawable(cxt
									.getResources().getDrawable(
											R.drawable.retirer));
							holder.parentLayout
									.setBackgroundResource(R.drawable.bande_element_2);
							holder.imgQuantite.setTag("2");
							holder.imgQuantite.setBackgroundDrawable(cxt
									.getResources().getDrawable(
											R.drawable.img_double));
						} else if (quantite == 1) {

							commande.get(inCommande).setQuantite(0);
							System.out.println("size Commande before remove  :"
									+ commande.size());
							commande.remove(inCommande);
							System.out.println("size Commande after remove  :"
									+ commande.size());

							holder.imgAddOrRemove.setBackgroundDrawable(cxt
									.getResources().getDrawable(
											R.drawable.ajouter));
							holder.parentLayout
									.setBackgroundResource(R.drawable.bande_element1);
							holder.imgQuantite.setTag("2");
							holder.imgQuantite.setBackgroundDrawable(cxt
									.getResources().getDrawable(
											R.drawable.img_double));
						}
					}
				}

				float price = 0;
				int calories = 0;
				for (int i = 0; i < commande.size(); i++) {

					price = price
							+ (Float.parseFloat(commande.get(i).getSalade()
									.getPrice()) * commande.get(i)
									.getQuantite());
					calories = calories
							+ Integer.parseInt((commande.get(i).getSalade()
									.getCalories()))
							* commande.get(i).getQuantite();
				}

				txt_totalPrice.setText(price + " dt");
				txt_totalCalorie.setText(calories + " cal");

				// holder.parentLayout.setBackgroundResource(R.drawable.bande_element_2);
			}
		});

		Salade current = getItem(position);
		int inCommande = -1;
		int quantiteCu = 0;
		for (int i = 0; i < commande.size(); i++) {

			if ((commande.get(i).getSalade().getId() == current.getId())
					&& (commande.get(i).getSalade().getFamily() == current
							.getFamily())) {
				inCommande = i;
				quantiteCu = commande.get(i).getQuantite();
			}
		}

		if (inCommande == -1) {
			holder.parentLayout
					.setBackgroundResource(R.drawable.bande_element1);
			holder.imgQuantite.setBackgroundDrawable(cxt.getResources()
					.getDrawable(R.drawable.img_double));
			holder.imgAddOrRemove.setBackgroundDrawable(cxt.getResources()
					.getDrawable(R.drawable.ajouter));
		} else {
			holder.parentLayout
					.setBackgroundResource(R.drawable.bande_element_2);
			if (quantiteCu == 1) {

				holder.imgQuantite.setBackgroundDrawable(cxt.getResources()
						.getDrawable(R.drawable.img_double));
				holder.imgAddOrRemove.setBackgroundDrawable(cxt.getResources()
						.getDrawable(R.drawable.retirer));
			} else {

				holder.imgQuantite.setBackgroundDrawable(cxt.getResources()
						.getDrawable(R.drawable.simple));
				holder.imgAddOrRemove.setBackgroundDrawable(cxt.getResources()
						.getDrawable(R.drawable.retirer));
			}

		}
		return view;
	}

	public static class ViewHolder {

		public TextView txt_name;
		public TextView txt_prix;
		public TextView txt_calorie;
		public ImageView imgSalade;
		public ImageView imgQuantite;
		public ImageView imgAddOrRemove;
		public RelativeLayout parentLayout;
	}

}
