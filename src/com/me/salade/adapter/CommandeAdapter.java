package com.me.salade.adapter;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Shader.TileMode;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.me.salade.R;
import com.me.salade.model.CommandeItems;

public class CommandeAdapter extends ArrayAdapter<CommandeItems> {

	private LayoutInflater inflater = null;
	private Context cxt;
	private ArrayList<CommandeItems> commande;
	private TextView txt_totalPrice;
	private TextView txt_totalCalorie;

	public CommandeAdapter(Context cxt, int resid,
			ArrayList<CommandeItems> commande, TextView txt_totalPrice,
			TextView txt_totalCalorie) {
		super(cxt, resid);
		this.cxt = cxt;
		this.commande = commande;

		this.txt_totalPrice = txt_totalPrice;
		this.txt_totalCalorie = txt_totalCalorie;

		inflater = (LayoutInflater) cxt
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		commande = new ArrayList<CommandeItems>();
	}

	public ArrayList<CommandeItems> getCommande() {
		return commande;
	}

	public void setItems(ArrayList<CommandeItems> commande) {
		this.commande = commande;
		notifyDataSetChanged();
	}

	@Override
	public int getCount() {
		return commande.size();
	}

	@Override
	public CommandeItems getItem(int position) {
		return commande.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(final int position, View view, ViewGroup parent) {

		System.out.println("it's here");
		final CommandeItems salade = commande.get(position);

		final ViewHolder holder;

		if (view == null) {
			holder = new ViewHolder();
			view = inflater.inflate(R.layout.commande_item_view, null);

			holder.txt_name = (TextView) view.findViewById(R.id.txt_nameItem);
			holder.txt_prix = (TextView) view.findViewById(R.id.txt_priceItem);
			holder.txt_calorie = (TextView) view
					.findViewById(R.id.txt_calorieItem);
			holder.imgSalade = (ImageView) view.findViewById(R.id.img_item);

			holder.imgAddOrRemove = (ImageView) view
					.findViewById(R.id.img_retirerAjouter);

			view.setTag(holder);
		}

		else {
			holder = (ViewHolder) view.getTag();
		}

		holder.txt_name.setText(salade.getSalade().getName());
		holder.txt_prix.setText(salade.getSalade().getPrice() + " dt");
		holder.txt_calorie.setText(salade.getSalade().getCalories() + " cal");

		URL url;
		try {
			url = new URL(salade.getSalade().getImage());

			Bitmap bmp = BitmapFactory.decodeStream(url.openConnection()
					.getInputStream());
			Bitmap circleBitmap = Bitmap.createBitmap(bmp.getWidth(),
					bmp.getHeight(), Bitmap.Config.ARGB_8888);

			BitmapShader shader = new BitmapShader(bmp, TileMode.CLAMP,
					TileMode.CLAMP);
			Paint paint = new Paint();
			paint.setShader(shader);

			Canvas c = new Canvas(circleBitmap);
			c.drawCircle(bmp.getWidth() / 2, bmp.getHeight() / 2,
					bmp.getWidth() / 2, paint);
			holder.imgSalade.setImageBitmap(circleBitmap);
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		holder.imgAddOrRemove.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				commande.remove(position);
				System.out.println("Size liste  :" + getCount());
				// remove(salade);
				notifyDataSetChanged();
				System.out.println("Size liste after :" + getCount());

				float price = 0;
				float calories = 0;
				for (int i = 0; i < commande.size(); i++) {

					price = price
							+ (Float.parseFloat(commande.get(i).getSalade()
									.getPrice()) * commande.get(i)
									.getQuantite());
					calories = calories
							+ Integer.parseInt((commande.get(i).getSalade()
									.getCalories()))
							* commande.get(i).getQuantite();
				}

				txt_totalPrice.setText(price + " dt");
				txt_totalCalorie.setText(calories + " cal");
			}
		});

		return view;
	}

	public static class ViewHolder {

		public TextView txt_name;
		public TextView txt_prix;
		public TextView txt_calorie;
		public ImageView imgSalade;
		public ImageView imgAddOrRemove;
	}

}
