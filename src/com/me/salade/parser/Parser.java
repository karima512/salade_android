package com.me.salade.parser;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.me.salade.ConnectActivity;
import com.me.salade.SendCommandeActivity;
import com.me.salade.model.Salade;


public class Parser {

	
	
	public static class LoginRequest extends AsyncTask<String, Context, String> {
		
	    public interface AsyncResponse {
	        void processFinish(String user);
	    }
	    
		private ProgressDialog dialog;
		
		// Creating JSON Parser object
		JSONParser jParser = new JSONParser();
		
		/** application context. */
		@SuppressWarnings("unused")
		private ConnectActivity activity;
		private Context context;
		public AsyncResponse asynk_result_state = null;
		    
		 public LoginRequest(ConnectActivity activity) {
		        this.activity = activity;
		        context = activity;
		        dialog = new ProgressDialog(context);
		    }
		 
	      @Override
	      protected void onPreExecute() {

	    	  this.dialog.setMessage("Loading");
	          this.dialog.show();
	      }
		 
		@Override
		protected String doInBackground(String... params) {
			
			String result;
			final String target_rest_url= params[0];
			
			// Building Parameters
			List<NameValuePair> jparams = new ArrayList<NameValuePair>();
			
			jparams.add(new BasicNameValuePair("email", params[1]));
			jparams.add(new BasicNameValuePair("code", params[2]));
			
			// getting JSON string from URL
			Log.i("UserData","Procress the request ...");
			result = jParser.makeHttpRequestString(target_rest_url, "POST", jparams);
	        result = result.replace("\n", "").replace("\r", "");
			System.out.println("do in background   :" + result);
			return result;
		}
		
		protected void onPostExecute(String result) {
						
			result.replaceAll("\\s+","");
			//Intialize the result at false, if valid user result will be true
			asynk_result_state.processFinish(result.toString());
			
				if (dialog.isShowing()) {
					dialog.dismiss();
				}
			
		}
		
	}
	
	
	//////////////////////////////////////////////////////////////////////////////////
	////////////////****************************************/////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	
	
	

public static class GetSaladesRequest extends AsyncTask<String, Context, String> {
	
    public interface SearshRequest {
        void processFinish(ArrayList<Salade> lstSalades);
    }
    
	private ProgressDialog dialog;
	
	// Creating JSON Parser object
	JSONParser jParser = new JSONParser();
	
	/** application context. */
	@SuppressWarnings("unused")
	private Activity activity;
	private Context context;
	public SearshRequest asynk_result_state = null;
	    
	 public GetSaladesRequest(Activity currentActivity) {
	        this.activity = currentActivity;
	        context = currentActivity;
	        dialog = new ProgressDialog(context);
	    }
	 
      @Override
      protected void onPreExecute() {
           
    	  this.dialog.setMessage("Loading");
          this.dialog.show();
      }
	 
	@Override
	protected String doInBackground(String... params) {
		
		String result;
		final String target_rest_url= params[0];
		
//		// Building Parameters
//		List<NameValuePair> jparams = new ArrayList<NameValuePair>();
//		
//		jparams.add(new BasicNameValuePair("uid", params[1]));
//		
//		Log.i("User ID", params[1]);
//		// getting JSON string from URL
//		Log.i("UserData","Procress the request ...");
//		Log.i("URL   :",target_rest_url);
		
		JSONArray json = jParser.makeHttpRequestJarray(target_rest_url, "GET", null);
		if(json != null){
			System.out.println("json is  :" + json.toString());
		}else
			System.out.println("json is  : nullll");
		result = json.toString();
		this.dialog.dismiss();
		return result;
	}
	
	protected void onPostExecute(String result) {
		
		ArrayList<Salade> salades = new ArrayList<Salade>();
		String error= null;
		
		//Intialize the result at false, if valid user result will be true
	//	asynk_result_state.processFinish(null);
		
		// PARSE jSON data
		try {
			JSONArray json_data = new JSONArray(result);
			if (dialog.isShowing()) {
				dialog.dismiss();
			}
			System.out.println("Json Result  :" +json_data.toString());


			
			for (int i=0; i<json_data.length(); i++) {
			    JSONObject salade = json_data.getJSONObject(i);
			    
			    String id = salade.getString("id");
				String name = salade.getString("name");
				String image = salade.getString("image");
				String price = salade.getString("prix");
				String family = salade.getString("famille");
				String calories = salade.getString("calories");
				
				Salade sal = new Salade(id, name, image, price, family, calories);
				salades.add(sal);
			    
			}
			
			
			// Return result to LoginActivity
			try {
				System.out.println("lst Salade Size   :" + salades.size());
				asynk_result_state.processFinish(salades);
			} catch (Exception ee) {
				Log.e("UserData",
						"Result Interface ERROR : "
								+ ee.getMessage());
			}
			
		} catch (JSONException e) {
			Log.e("UserData", "ERROR PARSING DATA " + e.toString());
		}
		
	}
	
}
	
	
	




public static class SendCommandRequest extends AsyncTask<String, Context, String> {
	
    public interface AsyncResponse {
        void processFinish(String user);
    }
    
	private ProgressDialog dialog;
	
	// Creating JSON Parser object
	JSONParser jParser = new JSONParser();
	
	/** application context. */
	@SuppressWarnings("unused")
	private SendCommandeActivity activity;
	private Context context;
	public AsyncResponse asynk_result_state = null;
	    
	 public SendCommandRequest(SendCommandeActivity activity) {
	        this.activity = activity;
	        context = activity;
	        dialog = new ProgressDialog(context);
	    }
	 
      @Override
      protected void onPreExecute() {

    	  this.dialog.setMessage("Loading");
          this.dialog.show();
      }
	 
	@Override
	protected String doInBackground(String... params) {
		
		String result;
		final String target_rest_url= params[0];
		
		//{ URL, price[0], source, ingredient, date, livraison, comment, userID});
		
		// Building Parameters
		List<NameValuePair> jparams = new ArrayList<NameValuePair>();

		jparams.add(new BasicNameValuePair("userid", params[7]));
		jparams.add(new BasicNameValuePair("prix", params[1]));
		jparams.add(new BasicNameValuePair("date", params[4]));
		jparams.add(new BasicNameValuePair("ingredient", params[3]));
		jparams.add(new BasicNameValuePair("message", params[6]));
		jparams.add(new BasicNameValuePair("source", params[2]));
		jparams.add(new BasicNameValuePair("livraison", params[5]));
		

		
		
		
		
		// getting JSON string from URL
		Log.i("UserData","Procress the request ...");
		result = jParser.makeHttpRequestString(target_rest_url, "POST", jparams);
        result = result.replace("\n", "").replace("\r", "");
		System.out.println("do in background  result � Commande :" + result);
		return result;
	}
	
	protected void onPostExecute(String result) {
					
		result.replaceAll("\\s+","");
		//Intialize the result at false, if valid user result will be true
		asynk_result_state.processFinish(result.toString());
		
			if (dialog.isShowing()) {
				dialog.dismiss();
			}
		
	}
	
}

}
