package com.me.salade.model;

import java.io.Serializable;

public class CommandeItems implements Serializable{
	
	private Salade salade;
	private int quantite;
	
	
	public CommandeItems() {
		super();
	}
	
	
	
	public CommandeItems(Salade salade, int quantite) {
		super();
		this.salade = salade;
		this.quantite = quantite;
	}




	public int getQuantite() {
		return quantite;
	}



	public void setQuantite(int quantite) {
		this.quantite = quantite;
	}



	public Salade getSalade() {
		return salade;
	}



	public void setSalade(Salade salade) {
		this.salade = salade;
	}
	
	
	
	
	

}
