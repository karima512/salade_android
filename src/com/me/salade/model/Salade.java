package com.me.salade.model;

import java.io.Serializable;

public class Salade implements Serializable{

	private String id;
	private String name;
	private String image;
	private String price;
	private String family;
	private String calories;
	
	
	
	public Salade() {
		super();
	}
	
	public Salade(String id, String name, String image, String price,
			String family, String calories) {
		super();
		this.id = id;
		this.name = name;
		this.image = image;
		this.price = price;
		this.family = family;
		this.calories = calories;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getFamily() {
		return family;
	}

	public void setFamily(String family) {
		this.family = family;
	}

	public String getCalories() {
		//calories = calories.substring(0, calories.length() - 1);
		return calories;
	}

	public void setCalories(String calories) {
		this.calories = calories;
	}
	
	
	
	
	
	
	
	
}
