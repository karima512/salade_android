package com.me.salade.model;

public class User {
	
	private String mail;
	private String clientID;
	
	
	public User() {
		super();
	}
	
	
	
	public User(String mail, String clientID) {
		super();
		this.mail = mail;
		this.clientID = clientID;
	}



	public String getMail() {
		return mail;
	}



	public void setMail(String mail) {
		this.mail = mail;
	}



	public String getClientID() {
		return clientID;
	}



	public void setClientID(String clientID) {
		this.clientID = clientID;
	}
	
	
	
	

}
