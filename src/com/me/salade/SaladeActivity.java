package com.me.salade;

import java.util.ArrayList;
import java.util.Arrays;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.me.salade.adapter.LstSaladeAdapter;
import com.me.salade.model.CommandeItems;
import com.me.salade.model.Salade;
import com.me.salade.parser.Parser;
import com.me.salade.parser.Parser.GetSaladesRequest.SearshRequest;

public class SaladeActivity extends Activity implements SearshRequest,
		OnClickListener {

	private ListView lstViewSalade;
	private TextView txtBase, txtToppings, txtSauce, txtPrice, txtCalorie;
	private ImageView imgGo, img_item;
	private LstSaladeAdapter adapter;
	private ArrayList<Salade> lstSaladeByCathegorie = new ArrayList<Salade>();
	private ArrayList<Salade> lstAllSalade = new ArrayList<Salade>();
	
	 String mail,  clientID, userID;
	 
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.salade_view);
		
		
		 Bundle extras = getIntent().getExtras();
		  if (extras != null) {
		   mail= extras.getString("mail");
		   clientID= extras.getString("clientID");
		   userID= extras.getString("userID");
		         System.out.println("UserID   :" + userID);
		}
		
		
		initView();
		String URL = "http://lvclubdiscount.tn/salade/composantesToDevice.php";

		Parser.GetSaladesRequest userData = new Parser.GetSaladesRequest(
				SaladeActivity.this);
		userData.execute(new String[] { URL });

		userData.asynk_result_state = this;

	}
	

	@Override
	public void processFinish(ArrayList<Salade> lstSalades) {
	
		lstAllSalade = lstSalades;
		for (int i = 0; i < lstSalades.size(); i++) {
			
			if (lstSalades.get(i).getFamily().equals("1")) {
				lstSaladeByCathegorie.add(lstSalades.get(i));
			}
		}
		System.out.println("lst Salade Size   :" + lstSaladeByCathegorie.size());
		adapter = new LstSaladeAdapter(getApplicationContext(), R.id.txt_nameItem,
				lstSaladeByCathegorie, txtPrice, txtCalorie);
		lstViewSalade.setAdapter(adapter);
		
	}
	
	

	public void initView() {

		lstViewSalade = (ListView) findViewById(R.id.lstSalade);
		txtBase = (TextView) findViewById(R.id.txt_base);
		txtToppings = (TextView) findViewById(R.id.txt_toppings);
		txtSauce = (TextView) findViewById(R.id.txt_Sauce);
		txtPrice = (TextView) findViewById(R.id.txt_price);
		txtCalorie = (TextView) findViewById(R.id.txt_calories);
		imgGo = (ImageView) findViewById(R.id.img_go);
		img_item = (ImageView) findViewById(R.id.img_item);

		txtBase.setOnClickListener(this);
		txtToppings.setOnClickListener(this);
		txtSauce.setOnClickListener(this);
		imgGo.setOnClickListener(this);
		
		
		txtToppings.setTextColor(Color.parseColor("#CCCCCC"));
		txtToppings.setTextSize(dpToPx(10));
		
		txtBase.setTextColor(Color.parseColor("#ffffff"));
		txtBase.setTextSize(dpToPx(12));
		
		txtSauce.setTextColor(Color.parseColor("#CCCCCC"));
		txtSauce.setTextSize(dpToPx(10));

	}
	
//    public void showAlertDialog(Context context, String title, String message, Boolean status) {
//        final AlertDialog alertDialog = new AlertDialog.Builder(context).create();
// 
//        // Setting Dialog Title
//        alertDialog.setTitle(title);
// 
//        // Setting Dialog Message
//        alertDialog.setMessage(message);
//         
//        // Setting alert dialog icon
//        alertDialog.setIcon(android.R.drawable.ic_dialog_alert);
//        
// 
//        // Setting OK Button
//        alertDialog.setButton("Retour", new DialogInterface.OnClickListener() {
//            public void onClick(DialogInterface dialog, int which) {
//            	alertDialog.dismiss();
//            }
//        });
// 
//        // Showing Alert Message
//        alertDialog.show();
//    }

	@Override
	public void onClick(View view) {
		
		switch (view.getId()) {
		
case R.id.txt_base:
			
			txtToppings.setTextColor(Color.parseColor("#ffffff"));
			txtToppings.setTextSize(dpToPx(10));
			
			txtBase.setTextColor(Color.parseColor("#CCCCCC"));
			txtBase.setTextSize(dpToPx(12));
			
			txtSauce.setTextColor(Color.parseColor("#ffffff"));
			txtSauce.setTextSize(dpToPx(10));
			
			lstSaladeByCathegorie.clear();
			for (int i = 0; i < lstAllSalade.size(); i++) {
				
				if (lstAllSalade.get(i).getFamily().equals("1")) {
					lstSaladeByCathegorie.add(lstAllSalade.get(i));
				}
			}
			System.out.println("lst Salade Size   :" + lstSaladeByCathegorie.size());
			adapter.setItems(lstSaladeByCathegorie);
			
			
			break;
			
		case R.id.txt_Sauce:
			
			txtToppings.setTextColor(Color.parseColor("#ffffff"));
			txtToppings.setTextSize(dpToPx(10));
			
			txtBase.setTextColor(Color.parseColor("#ffffff"));
			txtBase.setTextSize(dpToPx(10));
			
			txtSauce.setTextColor(Color.parseColor("#CCCCCC"));
			txtSauce.setTextSize(dpToPx(12));
			
			lstSaladeByCathegorie.clear();
			for (int i = 0; i < lstAllSalade.size(); i++) {
				
				if (lstAllSalade.get(i).getFamily().equals("3")) {
					lstSaladeByCathegorie.add(lstAllSalade.get(i));
				}
			}
			System.out.println("lst Salade Size   :" + lstSaladeByCathegorie.size());
			adapter.setItems(lstSaladeByCathegorie);
			break;
			
		case R.id.txt_toppings:
			
			txtToppings.setTextColor(Color.parseColor("#CCCCCC"));
			txtToppings.setTextSize(dpToPx(12));
			
			txtBase.setTextColor(Color.parseColor("#ffffff"));
			txtBase.setTextSize(dpToPx(10));
			
			txtSauce.setTextColor(Color.parseColor("#ffffff"));
			txtSauce.setTextSize(dpToPx(10));
			
			lstSaladeByCathegorie.clear();
			for (int i = 0; i < lstAllSalade.size(); i++) {
				
				if (lstAllSalade.get(i).getFamily().equals("4") || 
						lstAllSalade.get(i).getFamily().equals("5") ||
						lstAllSalade.get(i).getFamily().equals("6")) {
					lstSaladeByCathegorie.add(lstAllSalade.get(i));
				}
			}
			System.out.println("lst Salade Size   :" + lstSaladeByCathegorie.size());
			adapter.setItems(lstSaladeByCathegorie);
			
			break;
//		case R.id.txt_base:
//			
//			txtToppings.setTextColor(Color.parseColor("#CCCCCC"));
//			txtToppings.setTextSize(dpToPx(10));
//			
//			txtBase.setTextColor(Color.parseColor("#ffffff"));
//			txtBase.setTextSize(dpToPx(12));
//			
//			txtSauce.setTextColor(Color.parseColor("#CCCCCC"));
//			txtSauce.setTextSize(dpToPx(10));
//			
//			lstSaladeByCathegorie.clear();
//			for (int i = 0; i < lstAllSalade.size(); i++) {
//				
//				if (lstAllSalade.get(i).getFamily().equals("1")) {
//					lstSaladeByCathegorie.add(lstAllSalade.get(i));
//				}
//			}
//			System.out.println("lst Salade Size   :" + lstSaladeByCathegorie.size());
//			adapter.setItems(lstSaladeByCathegorie);
//			
//			
//			break;
//			
//		case R.id.txt_Sauce:
//			
//			txtToppings.setTextColor(Color.parseColor("#CCCCCC"));
//			txtToppings.setTextSize(dpToPx(10));
//			
//			txtBase.setTextColor(Color.parseColor("#CCCCCC"));
//			txtBase.setTextSize(dpToPx(10));
//			
//			txtSauce.setTextColor(Color.parseColor("#ffffff"));
//			txtSauce.setTextSize(dpToPx(12));
//			
//			lstSaladeByCathegorie.clear();
//			for (int i = 0; i < lstAllSalade.size(); i++) {
//				
//				if (lstAllSalade.get(i).getFamily().equals("3")) {
//					lstSaladeByCathegorie.add(lstAllSalade.get(i));
//				}
//			}
//			System.out.println("lst Salade Size   :" + lstSaladeByCathegorie.size());
//			adapter.setItems(lstSaladeByCathegorie);
//			break;
//			
//		case R.id.txt_toppings:
//			
//			txtToppings.setTextColor(Color.parseColor("#ffffff"));
//			txtToppings.setTextSize(dpToPx(12));
//			
//			txtBase.setTextColor(Color.parseColor("#CCCCCC"));
//			txtBase.setTextSize(dpToPx(10));
//			
//			txtSauce.setTextColor(Color.parseColor("#CCCCCC"));
//			txtSauce.setTextSize(dpToPx(10));
//			
//			lstSaladeByCathegorie.clear();
//			for (int i = 0; i < lstAllSalade.size(); i++) {
//				
//				if (lstAllSalade.get(i).getFamily().equals("4") || 
//						lstAllSalade.get(i).getFamily().equals("5") ||
//						lstAllSalade.get(i).getFamily().equals("6")) {
//					lstSaladeByCathegorie.add(lstAllSalade.get(i));
//				}
//			}
//			System.out.println("lst Salade Size   :" + lstSaladeByCathegorie.size());
//			adapter.setItems(lstSaladeByCathegorie);
//			
//			break;
			
		case R.id.img_go :
			
//			ArrayList<CommandeItems> commande = adapter.getCommande();
//			String[] family = new String[commande.size()];
//			
//			System.out.println("Commande Size    :" + commande.size());
//			for (int i = 0; i < commande.size(); i++) {
//				System.out.println("name Salade    :" + commande.get(i).getSalade().getName() 
//						+ "    && quantit�   :" + commande.get(i).getQuantite());
//			}
//			
//			for (int i = 0; i<commande.size(); i++){
//				family[i]=commande.get(i).getSalade().getFamily();
//			}
//			
//			if (!Arrays.asList(family).contains("1")){
//				showAlertDialog(SaladeActivity.this, "Attention",
//	                    "Attention, vous devez choisir une base! ", false);
//			}
//			else
//				{
//				Intent intent = new Intent(SaladeActivity.this, SendCommandeActivity.class);
//				intent.putExtra("commande", commande);
//				intent.putExtra("prix", txtPrice.getText().toString());
//				intent.putExtra("cal", txtCalorie.getText().toString());
//				
//				startActivity(intent);
//				}
			

			
			if(haveGategorie("1") && haveGategorie("3") && (haveGategorie("4") || haveGategorie("5")  || haveGategorie("6") ) ){
				ArrayList<CommandeItems> commande = adapter.getCommande();
				
				Intent intent = new Intent(SaladeActivity.this, SendCommandeActivity.class);
				intent.putExtra("commande", commande);
				intent.putExtra("prix", txtPrice.getText().toString());
				intent.putExtra("cal", txtCalorie.getText().toString());
				intent.putExtra("userID", userID);
				startActivity(intent);
				}
			else if(!haveGategorie("1")){
	           final AlertDialog alertDialog = new AlertDialog.Builder(SaladeActivity.this).create();;
            	alertDialog.setTitle("Attention");
				alertDialog.setMessage("Attention, vous devez choisir une base! ");
				alertDialog.setButton2("Ok",
						new Dialog.OnClickListener() {

							@Override
							public void onClick(
									DialogInterface dialog,
									int which) {
								alertDialog.dismiss();
							}

						});
				alertDialog.show();
			}
			else {
				if(!haveGategorie("4") && !haveGategorie("5") && !haveGategorie("6") ){
				
				final AlertDialog alertDialog = new AlertDialog.Builder(SaladeActivity.this).create();;
            	alertDialog.setTitle("Attention");
				alertDialog.setMessage("Voulez vous continuer sans choisir un topping ?");
				alertDialog.setButton("Oui",
						new Dialog.OnClickListener() {

							@Override
							public void onClick(
									DialogInterface dialog,
									int which) {
								ArrayList<CommandeItems> commande = adapter.getCommande();
								
								System.out.println("Commande Size    :" + commande.size());
								for (int i = 0; i < commande.size(); i++) {
									System.out.println("name Salade    :" + commande.get(i).getSalade().getName() 
											+ "    && quantit�   :" + commande.get(i).getQuantite());
								}
								

								Intent intent = new Intent(SaladeActivity.this, SendCommandeActivity.class);
								intent.putExtra("commande", commande);
								intent.putExtra("prix", txtPrice.getText().toString());
								intent.putExtra("cal", txtCalorie.getText().toString());
								intent.putExtra("userID", userID);
								startActivity(intent);
								alertDialog.dismiss();
								
							}

						});
				alertDialog.setButton2("non",
						new Dialog.OnClickListener() {

							@Override
							public void onClick(
									DialogInterface dialog,
									int which) {
								alertDialog.dismiss();
							}

						});
				
				alertDialog.show();
				}
				else if(!haveGategorie("3") ){
					
					final AlertDialog alertDialog = new AlertDialog.Builder(SaladeActivity.this).create();;
	            	alertDialog.setTitle("Attention");
					alertDialog.setMessage("Voulez vous continuer sans choisir une Sauce ?");
					alertDialog.setButton("Oui",
							new Dialog.OnClickListener() {

								@Override
								public void onClick(
										DialogInterface dialog,
										int which) {
									ArrayList<CommandeItems> commande = adapter.getCommande();
									
									System.out.println("Commande Size    :" + commande.size());
									for (int i = 0; i < commande.size(); i++) {
										System.out.println("name Salade    :" + commande.get(i).getSalade().getName() 
												+ "    && quantit�   :" + commande.get(i).getQuantite());
									}
									

									Intent intent = new Intent(SaladeActivity.this, SendCommandeActivity.class);
									intent.putExtra("commande", commande);
									intent.putExtra("prix", txtPrice.getText().toString());
									intent.putExtra("cal", txtCalorie.getText().toString());
									intent.putExtra("userID", userID);
									startActivity(intent);
									alertDialog.dismiss();
									
								}

							});
					alertDialog.setButton2("non",
							new Dialog.OnClickListener() {

								@Override
								public void onClick(
										DialogInterface dialog,
										int which) {
									alertDialog.dismiss();
								}

							});
					
					alertDialog.show();}
			}
			
			break;

		default:
			break;
		}

	}
	
	public int dpToPx(int dp) {
	    DisplayMetrics displayMetrics = getApplicationContext().getResources().getDisplayMetrics();
	    int px = Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));       
	    return px;
	}
	
	public boolean  haveGategorie(String idCategory){
		
		ArrayList<CommandeItems> commande = adapter.getCommande();
		for (int i = 0; i < commande.size(); i++) {
			if(commande.get(i).getSalade().getFamily().equals(idCategory))
				return true;
		}
		return false;
	}

}
