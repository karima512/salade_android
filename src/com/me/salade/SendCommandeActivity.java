package com.me.salade;

import java.util.ArrayList;
import java.util.Calendar;

import com.me.salade.adapter.CommandeAdapter;
import com.me.salade.model.CommandeItems;
import com.me.salade.model.Salade;
import com.me.salade.parser.Parser;
import com.me.salade.parser.Parser.GetSaladesRequest.SearshRequest;
import com.me.salade.parser.Parser.SendCommandRequest.AsyncResponse;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TimePicker;

public class SendCommandeActivity extends Activity implements OnClickListener, AsyncResponse {

	private TextView txt_price, txt_calorie;
	private ImageView img_sendCommande;
	//private Button btn_date;
	private ListView lstViewSalade;
	private static ArrayList<CommandeItems> commande;
	private static String prix;
	private static String calorie;
	private static CommandeAdapter adapt;
	
	private static String date, livraison, comment;
    static String userID;
    private static Activity context;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.send_commande_view);
		context = SendCommandeActivity.this;
		initView();

	}

	public void initView() {

		lstViewSalade = (ListView) findViewById(R.id.lstCommande);

		txt_price = (TextView) findViewById(R.id.txt_price);
		txt_calorie = (TextView) findViewById(R.id.txt_calories);
		img_sendCommande = (ImageView) findViewById(R.id.img_go);
//		btn_date = (Button) findViewById(R.id.btnDate);
//
//		btn_date.setOnClickListener(this);
		img_sendCommande.setOnClickListener(this);
		
		
		commande = (ArrayList<CommandeItems>) getIntent().getSerializableExtra("commande");
		System.out.println("Size commande sended   :" + commande.size());
		prix = getIntent().getStringExtra("prix");
		calorie = getIntent().getStringExtra("cal");
		userID = getIntent().getStringExtra("userID");
		
//		txt_price.setText("Prix :" + prix);
//		txt_calorie.setText("Calories :" + calorie);
		
		float price = 0;
		float calories = 0;
		for (int i = 0; i < commande.size(); i++) {
			
			price = price + ( Float.parseFloat(commande.get(i).getSalade().getPrice()) * commande.get(i).getQuantite() ); 
			calories  = calories + Integer.parseInt((commande.get(i).getSalade().getCalories())) * commande.get(i).getQuantite();
		}
       
       txt_price.setText(price + " dt");
       txt_calorie.setText(calories + " cal");
       
		adapt = new CommandeAdapter(getApplicationContext(), R.id.txt_nameItem, commande, txt_price, txt_calorie);
		lstViewSalade.setAdapter(adapt);

	}

	@Override
	public void onClick(View view) {
		switch (view.getId()) {
//		case R.id.btnDate:
//			showPopupEditTime(SendCommandeActivity.this);
//			break;

		case R.id.img_go :
			//if(date == null){
				showPopupEditTime(SendCommandeActivity.this);
			//}
//			else{
//
//				System.out.println(date);
//				if(livraison == null){
//					showPopupPlaceCommande(SendCommandeActivity.this);
//				}
//				else if(livraison != null && comment == null){
//					showPopupComment(SendCommandeActivity.this);
//				}
//				else if(livraison != null && comment != null){
//					System.out.println(" date  :" + date + "  && livraison  :" + livraison + "   && comment  :" + comment);
//				}
//			}
		break;
		
		
//		case R.id.img_go:
//
//			if(date == null){
//				new AlertDialog.Builder(this)
//			    .setTitle("LV CLUB")
//			    .setMessage("Vous devez tous dabord regler la date du commande")
//			    .setNegativeButton("Ok", new DialogInterface.OnClickListener() {
//			        public void onClick(DialogInterface dialog, int which) { 
//			           
//			        	dialog.dismiss();
//			        	showPopupEditTime(SendCommandeActivity.this);
//			        }
//			     })
//			    .setIcon(android.R.drawable.ic_dialog_alert)
//			     .show();
//			}
//			else
//			{
//				System.out.println("Commande Go size   :" + adapt.getCommande().size() + "date   :" + date);
//			
//				String URL = "http://lvclubdiscount.tn/salade/coming_comm_test.php";
//
//                 String[] price = prix.split("\\s+");
//                 String[] cal = calorie.split("\\s+");
//                 String ingredient ="";
//                 for (int i = 0; i < commande.size(); i++) {
//					if (i == 0) {
//
//	                	ingredient = commande.get(i).getSalade().getId() + ";" +
//	                			commande.get(i).getQuantite();
//					}
//					else
//                	ingredient = ingredient + "&&" + commande.get(i).getSalade().getId() + ";" +
//                			commande.get(i).getQuantite();
//				}
//                 System.out.println("price  :" + price[0]);
//                 System.out.println("cal  :" + cal[0]);
//                 System.out.println("ingredient  :" + ingredient);
//                 
//				Parser.SendCommandRequest userData = new Parser.SendCommandRequest(
//						SendCommandeActivity.this);
//				userData.execute(new String[] { URL, price[0], cal[0], ingredient});
//				userData.asynk_result_state = this;
//			
//			}
//			break;

		default:
			break;
		}
	}
	
	
	public static void showPopupEditTime(Activity activite){//, final TextView txt_time,  final TextView txt_date) {
		try {
			
			final Dialog picker = new Dialog(activite);
             picker.setContentView(R.layout.date_time_picker);
             picker.setTitle("Choisir la date du commande");
             
             final DatePicker datep = (DatePicker)picker.findViewById(R.id.datePicker);
             final TimePicker timep = (TimePicker)picker.findViewById(R.id.timePicker1);
              timep.setIs24HourView(true);
              Calendar calendar = Calendar.getInstance();

              int h = calendar.get(Calendar.HOUR_OF_DAY);
              int m = calendar.get(Calendar.MINUTE);

              timep.setCurrentHour(h);
              timep.setCurrentMinute(m);
             Button set = (Button)picker.findViewById(R.id.btnSet);

             set.setOnClickListener(new View.OnClickListener() {

                 @Override
                 public void onClick(View view) {
                	 Integer hour,minute,month,day,year;
                     month = datep.getMonth()+1;
                     day = datep.getDayOfMonth();
                     year = datep.getYear();
                     hour = timep.getCurrentHour();
                     minute = timep.getCurrentMinute();
                     date = String.format("%02d-%02d",day,month)+"-"+year + "," + String.format("%02d:%02d", hour,minute);
                     picker.dismiss();
                     
                 	
        				System.out.println(date);
        				//if(livraison == null){
        					showPopupPlaceCommande((SendCommandeActivity) context);
        				//}
        				        			
                 }
             });
             picker.show();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void processFinish(String result) {
		
		System.out.println("process is finished   :" + result);
		
//		new AlertDialog.Builder(this)
//	    .setTitle("LV CLUB")
//	    .setMessage("Votre commande a et� envoyer avec succ�es !")
//	    .setNegativeButton("Ok", new DialogInterface.OnClickListener() {
//	        public void onClick(DialogInterface dialog, int which) { 
//	           
//	        	dialog.dismiss();
//	        	startActivity(new Intent(SendCommandeActivity.this, SaladeActivity.class));
//	        }
//	     })
//	    .setIcon(android.R.drawable.ic_dialog_alert)
//	     .show();
		
      try {
			
			final Dialog popup = new Dialog(SendCommandeActivity.this);
			popup.setContentView(R.layout.popup_code_commande);
            popup.setTitle("Code Commande :");
             final TextView txt_code = (TextView) popup.findViewById(R.id.txt_codeComment);
             final Button btn_go = (Button)popup.findViewById(R.id.btn_commentaire);
              txt_code.setText("" + result);
             btn_go.setOnClickListener(new View.OnClickListener() {

                 @Override
                 public void onClick(View view) {
                	 popup.dismiss();
                	 startActivity(new Intent(SendCommandeActivity.this, SaladeActivity.class));
                	 SendCommandeActivity.this.finish();
                 }
             });
             
             
             popup.show();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	public static void showPopupPlaceCommande(Activity activite){
		try {
			
			final Dialog popup = new Dialog(activite);
			popup.setContentView(R.layout.popup_lieu_commande);
            popup.setTitle("Vous allez consommez votre salade");
             final TextView txt_surPlace = (TextView)popup.findViewById(R.id.txt_surPlace);
             final TextView txt_emporter = (TextView)popup.findViewById(R.id.txt_emporter);
              
             txt_surPlace.setOnClickListener(new View.OnClickListener() {

                 @Override
                 public void onClick(View view) {
                	 livraison = "place";
                	 popup.dismiss();
                	
     					showPopupComment((SendCommandeActivity) context);
     			
                 }
             });
             
             txt_emporter.setOnClickListener(new View.OnClickListener() {

                 @Override
                 public void onClick(View view) {
                	 livraison ="emporter";
                	 popup.dismiss();
                	 
                	 showPopupComment((SendCommandeActivity) context);
                 }
             });
             
             
             popup.show();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	public static void showPopupComment(Activity activite){
		try {
			
			final Dialog popup = new Dialog(activite);
			popup.setContentView(R.layout.popup_comment);
            popup.setTitle("Saisissez ici votre Commentaire au chef");
             final EditText txt_commente = (EditText)popup.findViewById(R.id.edit_commentaire);
             final Button btn_go = (Button)popup.findViewById(R.id.btn_commentaire);
              
             btn_go.setOnClickListener(new View.OnClickListener() {

                 @Override
                 public void onClick(View view) {
                	 comment = txt_commente.getText().toString();
                	 
                	 
                	 
     				System.out.println("Commande Go size   :" + adapt.getCommande().size() + "date   :" + date);
         			
         				String URL = "http://lvclubdiscount.tn/salade/coming_comm_test.php";
         
         				float price = 0;
        				float calories = 0;
        				for (int i = 0; i < commande.size(); i++) {
        					
        					price = price + ( Float.parseFloat(commande.get(i).getSalade().getPrice()) * commande.get(i).getQuantite() ); 
        					calories  = calories + Integer.parseInt((commande.get(i).getSalade().getCalories())) * commande.get(i).getQuantite();
        				}
        		       
                          String ingredient ="";
                          for (int i = 0; i < commande.size(); i++) {
         					if (i == 0) {
         
         	                	ingredient = commande.get(i).getSalade().getId() + ";" +
         	                			commande.get(i).getQuantite();
         					}
         					else
                         	ingredient = ingredient + "&&" + commande.get(i).getSalade().getId() + ";" +
                         			commande.get(i).getQuantite();
         				}
                          System.out.println("price  :" + price);
                          System.out.println("cal  :" + calories);
                          System.out.println("ingredient  :" + ingredient);
                          String source = "android";
         				Parser.SendCommandRequest userData = new Parser.SendCommandRequest((SendCommandeActivity)context);
         				userData.execute(new String[] { URL, price+"", source, ingredient, date, livraison, comment, userID});
         				userData.asynk_result_state = (SendCommandeActivity)context;
                	 
                	 
                	 
                	 popup.dismiss();
                 }
             });
             
             
             popup.show();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	public static void showPopupCodeCommande(Activity activite){
		try {
			
			final Dialog popup = new Dialog(activite);
			popup.setContentView(R.layout.popup_code_commande);
            popup.setTitle("Code Commande :");
             final TextView txt_code = (TextView) popup.findViewById(R.id.txt_codeComment);
             final Button btn_go = (Button)popup.findViewById(R.id.btn_commentaire);
              
             btn_go.setOnClickListener(new View.OnClickListener() {

                 @Override
                 public void onClick(View view) {
                	 popup.dismiss();
                 }
             });
             
             
             popup.show();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

//	Key :"userid"  value" "
//	Key :"prix", value " "
//	Key "date" value : "21/12/2013"
//	Key : "ingredient" value sous format d'un seul String concatin� : " idSalade;quantite&&idSalade;quantite&&idSalade;quantite "
//	Key :"message" value : " String tap� par l'utilisateur "
//	Key :"source" value : "android"
//	Key :"livraison" value: "emporter ou bien place"
}
