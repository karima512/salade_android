package com.me.salade;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.me.salade.model.User;
import com.me.salade.parser.Parser;
import com.me.salade.parser.Parser.LoginRequest.AsyncResponse;

public class ConnectActivity extends Activity implements OnClickListener,
		AsyncResponse {

	private EditText editMail, editClientID;
	private Button btnConnect;

	String user_email;
	String clientID;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		editMail = (EditText) findViewById(R.id.editMAil);
		editClientID = (EditText) findViewById(R.id.editClientID);
		btnConnect = (Button) findViewById(R.id.btnConnect);
		btnConnect.setOnClickListener(this);

	}

	@Override
	public void onClick(View view) {

		if (view.getId() == R.id.btnConnect) {

			String URL = "http://lvclubdiscount.tn/salade/checkUser.php";
			user_email = editMail.getText().toString();
			clientID = editClientID.getText().toString();

			Parser.LoginRequest userData = new Parser.LoginRequest(
					ConnectActivity.this);
			userData.execute(new String[] { URL, user_email, clientID });
			userData.asynk_result_state = this;

			// Hide keyboard
			InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
			inputManager.hideSoftInputFromWindow(getCurrentFocus()
					.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
		}
	}

	@Override
	public void processFinish(String user) {
		System.out.println("user   :" + user);
		
		
		if (user !=  null && !user.equals("")) {
			int result = Integer.parseInt(user);
			Bundle bundle = new Bundle();
			bundle.putString("mail", user_email);
			bundle.putString("clientID", clientID);
			bundle.putString("userID", user);
			Intent intent = new Intent(ConnectActivity.this,
					SaladeActivity.class);
			intent.putExtras(bundle);
			startActivity(intent);
			ConnectActivity.this.finish();
		} else {
			Toast.makeText(getApplicationContext(), "Erreur de connexion !!",
					Toast.LENGTH_LONG).show();
		}
	}

}
